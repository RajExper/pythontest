from setuptools import setup, find_packages

setup(
    name='module1',
    version='0.0.1',
    url='https://gitlab.com/RajExper/pythontest',
    author='Author Name',
    author_email='author@gmail.com',
    description='Description of my package',
    packages=find_packages(),
    install_requires=[ ],
    extras_require={
        "dev":[
            "pytest>=6.0",
        ],
    },
)
